import puppeteer from "puppeteer";
import { Cluster } from "puppeteer-cluster";
import _ from "lodash";

const scrapeLots = async () => {
  let scrapeResult = {
      list: [],
    },
    scrapeQueue = [];

  console.log(`\n[#] STARTING SCRAPER [#]`);
  const initTime = new Date().getTime();

  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_CONTEXT,
    maxConcurrency: 8,
  });

  // TEMPLATES FOR ITEM LIST SCRAPING
  const scrapeTemplates = {
    scrapeItemList: async (page, url) => {
      let waitForList,
        itemList = [],
        getItemsInPage;
      switch (true) {
        case /www.leiloesjudiciais.com.br/.test(url):
          try {
            await page.goto(url);

            await page.click("#c-btn-pesquisa-avancada");

            await page.evaluate(() => {
              document.getElementById("c-pesquisa-categoria-14").checked = true; //categoria: fazenda
              document.getElementById("c-npt-status-1").checked = true; //status: aberto
              document.getElementById("cj-valor-minimo").value = 750000; //valor mínimo: 750 mil
            });

            waitForList = await Promise.all([
              page.waitForNavigation(),
              page.evaluate(() =>
                document.getElementById("cj-form-avancada").submit()
              ),
            ]);

            getItemsInPage = async () => {
              return await page.evaluate(() =>
                Array.from(
                  document.querySelectorAll(".c-lote .c-lote-descricao > a")
                ).map((e) => ({
                  url: e.href, //.replace(/(https:\/\/)/, "http://"),
                }))
              );
            };

            itemList = itemList.concat(await getItemsInPage());
            let nextPage = await page.evaluate(() =>
              document.querySelector(".c-pesquisa-avancada-paginacao .next > a")
            );
            while (nextPage) {
              waitForList = await Promise.all([
                page.waitForNavigation(),
                page.click(".c-pesquisa-avancada-paginacao .next > a"),
              ]);
              itemList = itemList.concat(await getItemsInPage());
              nextPage = await page.evaluate(() =>
                document.querySelector(
                  ".c-pesquisa-avancada-paginacao .next > a"
                )
              );
            }
            return itemList;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping items from ${url}: ${error} [!!!!]`
            );
          }
          break;

        case /www.prosperarleiloes.com.br/.test(url):
          try {
            await page.goto(url);

            await page.click(".pesquisa");

            await page.evaluate(() => {
              document.getElementById(
                "c-pesquisa-categoria-222"
              ).checked = true; //categoria: fazenda
              document.getElementById("c-npt-status-1").checked = true; //status: aberto
              document.getElementById("valor_minimo").value = 750000; //valor mínimo: 750 mil
            });

            waitForList = await Promise.all([
              page.waitForNavigation(),
              page.evaluate(() =>
                document.getElementById("cj-form-avancada").submit()
              ),
            ]);

            getItemsInPage = async () => {
              return await page.evaluate(() =>
                Array.from(
                  document.querySelectorAll(".c-lote .c-lote-descricao > a")
                ).map((e) => ({
                  url: e.href, //.replace(/(https:\/\/)/, "http://"),
                }))
              );
            };

            itemList = itemList.concat(await getItemsInPage());

            return itemList;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping items from ${url}: ${error} [!!!!]`
            );
          }
          break;

        case /www.zukerman.com.br/.test(url):
          try {
            await page.goto(url);

            getItemsInPage = async () => {
              return await page.evaluate(() =>
                Array.from(
                  document.querySelectorAll(".s-it-main .cd-0 .cd-it-lk")
                ).map((e) => ({
                  url: e.href, //.replace(/(https:\/\/)/, "http://"),
                }))
              );
            };

            itemList = itemList.concat(await getItemsInPage());

            return itemList;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping items from ${url}: ${error} [!!!!]`
            );
          }
          break;

        case /www.jeleiloes.com.br/.test(url):
          try {
            await page.goto(url);

            getItemsInPage = async () => {
              return await page.evaluate(() =>
                Array.from(
                  document.querySelectorAll(".box-resultado-pesquisa > a")
                ).map((e) => ({
                  url: e.href, //.replace(/(https:\/\/)/, "http://"),
                }))
              );
            };

            itemList = itemList.concat(await getItemsInPage());

            return itemList;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping items from ${url}: ${error} [!!!!]`
            );
          }
          break;

        default:
          return "no-template";
          break;
      }
    },
    scrapeContent: async ({ page, data }) => {
      const { url, i } = data;
      // console.log(`\n[##] SCRAPE ITEM ${i} CONTENT ${url} [##]\n`);
      let itemContent = {},
        noTemplate = false,
        noData = false;
      itemContent.url = url;
      switch (true) {
        case /www.leiloesjudiciaisrs.com.br/.test(url):
        case /www.deonizialeiloes.com.br/.test(url):
        case /www.mariafixerleiloes.com.br/.test(url):
        case /www.leiloesjudiciaisgo.com.br/.test(url):
        case /www.dmleiloesjudiciais.com.br/.test(url):
        case /www.leiloesjudiciaismg.com.br/.test(url):
        case /www.dinizmartinsleiloes.com.br/.test(url):
        case /www.balbinoleiloes.com.br/.test(url):
        case /www.fabioleiloes.com.br/.test(url):
        case /www.cidafixerleiloes.com.br/.test(url):
        case /www.leiloesjudiciaismgnorte.com.br/.test(url):
        case /www.carloferrarileiloes.com.br/.test(url):
        case /www.danieloliveiraleiloes.com.br/.test(url):
          try {
            await page.goto(url);
            // console.log(`--- begin scraping content of ${url}`);
            itemContent.status =
              (await page.$eval("#l-status", (e) => e.textContent)) || null;
            itemContent.cidade =
              (await page.$eval("#l-lote-descricao > p", (e) =>
                e.textContent.match(/[\sA-Z'ÃÁÀÂÉÈÊÍÌÓÒÔÚÙ]+\/\w{2}/)[0].trim()
              )) || null;
            itemContent.descricao =
              (await page.$eval(
                "#l-lote-descricao > p",
                (e) => e.textContent
              )) || null;
            itemContent.valorAvaliacao =
              (await page.$$eval("#l-cabecalho-infos-valores > li", (e) =>
                Number(
                  e
                    .filter((e) =>
                      e.querySelector("span").textContent.match("Avaliação")
                    )[0]
                    ?.querySelector("strong")
                    .textContent.replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;
            itemContent.valorLance =
              (await page.$$eval("#l-cabecalho-infos-valores > li", (e) =>
                Number(
                  e
                    .filter((e) =>
                      e.querySelector("span").textContent.match("Lance")
                    )[0]
                    ?.querySelector("strong")
                    .textContent.replace("R$ ", "")
                    .replace(`R$\n`, "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;
            itemContent.valorIncremento =
              (await page.$$eval("#l-cabecalho-infos-valores > li", (e) =>
                Number(
                  e
                    .filter((e) =>
                      e.querySelector("span").textContent.match("Incremento")
                    )[0]
                    ?.querySelector("strong")
                    .textContent.replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping content of ${url}: ${error} [!!!!]`
            );
          }
          break;

        case /www.hdleiloes.com.br/.test(url):
          try {
            await page.goto(url);
            itemContent.status =
              (await page.$eval(
                ".c-detalhes-bem-status",
                (e) => e.textContent
              )) || null;

            itemContent.cidade =
              (await page.$eval(".c-detalhes-bem-descricao-lote > p", (e) =>
                e.textContent.match(/[\sA-Z'ÃÁÀÂÉÈÊÍÌÓÒÔÚÙ]+\/\w{2}/)[0].trim()
              )) || null;

            itemContent.descricao =
              (await page.$eval(
                ".c-detalhes-bem-descricao-lote > p",
                (e) => e.textContent
              )) || null;

            itemContent.valorAvaliacao =
              (await page.$$eval(".c-detalhes-bem-valores tr", (e) =>
                Number(
                  e
                    .filter((e) =>
                      e.querySelector("th").textContent.match("Avaliação")
                    )[0]
                    ?.querySelector("td")
                    .textContent.replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;

            itemContent.valorLance =
              (await page.$$eval(".c-detalhes-bem-valores tr", (e) =>
                Number(
                  e
                    .filter((e) =>
                      e.querySelector("th").textContent.match("Lance mínimo")
                    )[0]
                    ?.querySelector("td")
                    .textContent.replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;

            itemContent.valorIncremento =
              (await page.$$eval(".c-detalhes-bem-valores tr", (e) =>
                Number(
                  e
                    .filter((e) =>
                      e.querySelector("th").textContent.match("Incremento")
                    )[0]
                    ?.querySelector("td")
                    .textContent.replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping content of ${url}: ${error} [!!!!]`
            );
          }
          break;

        case /www.prosperarleiloes.com.br/.test(url):
          try {
            await page.goto(url);
            itemContent.status =
              (await page.$eval(
                ".c-leiloes .badge-success",
                (e) => e.textContent
              )) || null;

            /** NAO HA REFERENCIA OBJETIVA PARA CIDADE */
            itemContent.cidade = null;

            itemContent.descricao =
              (await page.evaluate(
                () =>
                  Array.from(document.querySelectorAll(".c-leiloes h6")).filter(
                    (e) => e.textContent == "Descrição do bem:"
                  )[0].nextElementSibling.textContent
              )) || null;

            itemContent.valorAvaliacao =
              (await page.evaluate(() =>
                Number(
                  Array.from(document.querySelectorAll(".c-leiloes b"))
                    .filter((e) => e.textContent == "Avaliação: ")[0]
                    .parentElement.textContent.replace("Avaliação: ", "")
                    .replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;

            itemContent.valorLance =
              (await page.evaluate(() =>
                Number(
                  Array.from(document.querySelectorAll(".c-leiloes b"))
                    .filter((e) => e.textContent == "Lance Mínimo: ")[0]
                    .parentElement.textContent.replace("Lance Mínimo: ", "")
                    .replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;

            itemContent.valorIncremento =
              (await page.evaluate(() =>
                Number(
                  Array.from(document.querySelectorAll(".c-leiloes b"))
                    .filter((e) => e.textContent == "Incremento: ")[0]
                    .parentElement.textContent.replace("Incremento: ", "")
                    .replace("R$ ", "")
                    .replace(/\./g, "")
                    .replace(",", ".")
                    .trim()
                )
              )) || null;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping content of ${url}: ${error} [!!!!]`
            );
          }
          break;

        // case /www.zukerman.com.br/.test(url):
        //   try {
        //     await page.goto(url);
        //     itemContent.status = null;

        //     itemContent.cidade = null;

        //     itemContent.descricao = null;

        //     itemContent.valorAvaliacao = null;

        //     itemContent.valorLance = null;

        //     itemContent.valorIncremento = null;
        //   } catch (error) {
        //     console.log(
        //       `[!!!!] Failed scraping content of ${url}: ${error} [!!!!]`
        //     );
        //   }
        //   break;

        case /www.jeleiloes.com.br/.test(url):
          try {
            await page.goto(url);
            itemContent.status =
              (await page.$eval(
                "#div-detalhes-lote-pc #Status",
                (e) => e.textContent
              )) || null;

            itemContent.cidade = await page.evaluate(
              () =>
                /.+ em (.+\/\w{2})/.exec(
                  document.querySelector(".header-titulo-leilao > h1")
                    .textContent
                )[1]
            );

            // itemContent.descricao =
            //   (await page.evaluate(
            //     () =>
            //       Array.from(document.querySelectorAll(".c-leiloes h6")).filter(
            //         (e) => e.textContent == "Descrição do bem:"
            //       )[0].nextElementSibling.textContent
            //   )) || null;

            // itemContent.valorAvaliacao =
            //   (await page.evaluate(() =>
            //     Number(
            //       Array.from(document.querySelectorAll(".c-leiloes b"))
            //         .filter((e) => e.textContent == "Avaliação: ")[0]
            //         .parentElement.textContent.replace("Avaliação: ", "")
            //         .replace("R$ ", "")
            //         .replace(/\./g, "")
            //         .replace(",", ".")
            //         .trim()
            //     )
            //   )) || null;

            // itemContent.valorLance =
            //   (await page.evaluate(() =>
            //     Number(
            //       Array.from(document.querySelectorAll(".c-leiloes b"))
            //         .filter((e) => e.textContent == "Lance Mínimo: ")[0]
            //         .parentElement.textContent.replace("Lance Mínimo: ", "")
            //         .replace("R$ ", "")
            //         .replace(/\./g, "")
            //         .replace(",", ".")
            //         .trim()
            //     )
            //   )) || null;

            // itemContent.valorIncremento =
            //   (await page.evaluate(() =>
            //     Number(
            //       Array.from(document.querySelectorAll(".c-leiloes b"))
            //         .filter((e) => e.textContent == "Incremento: ")[0]
            //         .parentElement.textContent.replace("Incremento: ", "")
            //         .replace("R$ ", "")
            //         .replace(/\./g, "")
            //         .replace(",", ".")
            //         .trim()
            //     )
            //   )) || null;
          } catch (error) {
            console.log(
              `[!!!!] Failed scraping content of ${url}: ${error} [!!!!]`
            );
          }
          break;

        default:
          noTemplate = true;
          break;
      }
      if (
        itemContent.status &&
        itemContent.cidade &&
        itemContent.descricao &&
        itemContent.valorAvaliacao &&
        itemContent.valorLance &&
        itemContent.valorIncremento
      ) {
        console.log(`[###] GOT COMPLETE DATA FROM ${url} [${i}] [###]`);
      } else if (
        itemContent.status ||
        itemContent.cidade ||
        itemContent.descricao ||
        itemContent.valorAvaliacao ||
        itemContent.valorLance ||
        itemContent.valorIncremento
      ) {
        console.log(`[#-#] GOT INCOMPLETE DATA FROM ${url} [${i}] [#/#]`);
      } else if (noTemplate) {
        console.log(`[???] MISSING TEMPLATE FOR ${url} [${i}] [???]`);
      } else {
        noData = true;
        console.log(`[---] GOT NO DATA FROM ${url} [${i}] [---]`);
      }
      // UPDATE ITEM DATA WITH CONTENT IF THERES DATA
      if (!noData && !noTemplate) {
        scrapeResult.list = scrapeResult.list.map((e) => {
          if (e.url == itemContent.url) {
            return itemContent;
          } else {
            return e;
          }
        });
      } else {
        // REMOVE ITEM IF THERES WAS NO DATA
        scrapeResult.list = scrapeResult.list.filter(
          (e) => e.url != itemContent.url
        );
      }
      // REMOVE ITEM FROM SCRAPE QUEUE
      scrapeQueue = scrapeQueue.filter((item) => item.url != url);
    },
  };

  await cluster.task(async ({ page, data }) => {
    const { sourceUrl } = data;
    // SCRAPE ITEM LIST FROM EACH SOURCE
    try {
      await page.goto(sourceUrl, { waitUntil: "domcontentloaded" });

      let curSrcList =
        (await scrapeTemplates.scrapeItemList(page, sourceUrl)) || [];

      if (curSrcList === "no-template") {
        console.log(
          `\n[!!] MISSING TEMPLATE FOR ITEMS SOURCE ${sourceUrl} [!!]`
        );
      } else {
        const curSrcListCount = curSrcList.length;
        // FILTER REPEATED ITEMS
        curSrcList = curSrcList.filter(
          (item) => !_.some(scrapeResult.list, item)
        );
        // ADD LIST INT CONTENT SCRAPE QUEUE
        scrapeQueue = curSrcList;
        // ADD LIST TO RESULTS OBJECT
        scrapeResult.list = scrapeResult.list.concat(curSrcList);
        console.log(
          `[##] ${curSrcList.length} ITEM(S) FOUND IN ${sourceUrl}${
            curSrcList.length < curSrcListCount
              ? ` (EXCLUDING ${
                  curSrcListCount - curSrcList.length
                } REPEATED) [##]`
              : " [##]"
          }`
        );
      }
    } catch (error) {
      console.log(
        `[!!] SCRAPE ITEM LIST FROM ${sourceUrl} FAILED. Error: ${error} [!!]`
      );
    }

    try {
      scrapeQueue.forEach(({ url }, i) => {
        // SCRAPE CONTENT FROM EACH ITEM
        cluster.queue(
          {
            url,
            i,
          },
          scrapeTemplates.scrapeContent
        );
      });
    } catch (error) {
      console.log(`[!!] SCRAPE ITEMS CONTENT FAILED. Error: ${error} [!!]`);
    }
  });

  //SOURCES
  cluster.queue({
    sourceUrl: "https://www.leiloesjudiciais.com.br",
  });
  cluster.queue({
    sourceUrl: "https://www.prosperarleiloes.com.br",
  });
  // cluster.queue({
  //   sourceUrl: "https://www.zukerman.com.br/areas-rurais",
  // });
  cluster.queue({
    sourceUrl: "https://www.lottileiloes.com.br/categorias/3",
  });
  cluster.queue({
    sourceUrl:
      "https://www.jeleiloes.com.br/PesquisaCategoria.aspx?categoria=33&faixasPreco=750000,0",
  });

  await cluster.idle();

  // const contentCountReducer = (acc, cur, idx) => acc + (cur.status ? 1 : 0);
  console.log(
    `
[#] ------------ RESULTS ------------ [#]\n
    - GOT ${scrapeResult.list.length} ITEMS
    - TIME SPENT: ${(new Date().getTime() - initTime) / 1000} seconds
    - RETURNED DATA:\n`,
    scrapeResult
  );
  console.log(
    `~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
  );
  await cluster.close();
};
scrapeLots();
